const axios = require('axios');
const fs = require('fs');
var options = require('./credentials.json')

var DRY_RUN = false;

async function getAll(query) {
    try {
        console.log(query)
        var res = await axios.get(query,options)
    } catch (e) {
        console.log(e.message)
        console.log(e.response.data)
    }
    var data = res.data.d.results
    var next = res.data.d.__next
    console.log(`Fetched ${data.length}`)
    if (res.headers['set-cookie']) {
        var token = res.headers['x-csrf-token']
        var session = res.headers['set-cookie'][0].split(';')[0]
        if (token && session) {
            options.headers = {
                'x-csrf-token': token,
                Cookie: session
            }
        }
    }
    while (next) {
        res = await axios.get(next,options)
        data = data.concat(res.data.d.results)
        next = res.data.d.__next || null
        console.log(`Fetched ${data.length}`)
    }
    console.log(`Finished query. Got ${data.length} records`)
    return data
}

async function postAll(payload,method = 'upsert') {
    console.log(`Sending payload of ${payload.length} entries`)
    var chunkSize = 25
    console.log(`Chunk size is ${chunkSize}`)
    for (var index = 0; index < payload.length; index += chunkSize) {
        var chunk = payload.slice(index, index+chunkSize);
        try {
            console.log(`Sending ${index+1} - ${index+chunkSize}`)
            var res = await axios.post(method,chunk,options)
            if (res.data) {
                if (res.data.d.filter(e => e.status==='ERROR').length) {
                    console.log(JSON.stringify(res.data.d,null,4))
                    process.exit(1)
                } else {
                    console.log('Done')
                }
            }
        } catch (e) {
            console.log(e.message)
            process.exit(1)
        }
    }
}

var requisitionFields = [
    { requisitionField: 'isDraft', mappingFunction: e => true, getter: e => true },
    //{ requisitionField: 'customFilter2', mappingFunction: e => e.cust_vacTypeStudentNav?.externalCode },
    { selectField: 'legalEntity_obj/externalCode', requisitionField: 'legalEntity_obj', mappingFunction: e => ({externalCode: e.companyNav ? e.companyNav.externalCode: null}), getter: e =>  e.legalEntity_obj ? e.legalEntity_obj.externalCode : null, newReqValueGetter: e => e.externalCode},
    { requisitionField: 'division', mappingFunction: e => e.companyNav.name_defaultValue},
    { selectField: 'jobReqLocale/jobTitle', requisitionField: 'jobReqLocale', mappingFunction: e => ({locale: 'ru_RU', jobTitle: e.externalName_defaultValue}), getter: e =>  e.jobReqLocale && e.jobReqLocale.results[0] ? e.jobReqLocale.results[0].jobTitle : null, newReqValueGetter: e => e.jobTitle },
    { requisitionField: 'poscode', mappingFunction: e => e.cust_positionCode },
    {  selectField: 'department_obj/externalCode', requisitionField: 'department_obj', mappingFunction: e => ({externalCode: e.department}), getter: e =>  e.department_obj? e.department_obj.externalCode : null, newReqValueGetter: e => e.externalCode},
    { requisitionField: 'grade', mappingFunction: e => e.cust_Grade },
    { requisitionField: 'rank', mappingFunction: e => e.cust_rank },
    { requisitionField: 'location1', mappingFunction: e => e.departmentNav.cust_address },
    { requisitionField: 'workMode2', mappingFunction: e => e.cust_jobCategory },
    { requisitionField: 'positionNumber', mappingFunction: e => e.code },
    {
        requisitionField: 'std_position_objlist',
        mappingFunction: e => ([{
            isPrimary: true,
            __metadata: {
                "type": "SFOData.JobReqGOPosition",
                "uri": "JobReqGOPosition"
            },
            value: {
                code: e.code
            }
        }]),
        getter: e => (e && e.results && e.results[0] && e.results[0].value && e.results[0].value.code ? e.results[0].value.code: null),
        newReqValueGetter: e => e[0].value.code
    },
    { requisitionField: 'vacation', mappingFunction: e => e.cust_vacationBase },
    { requisitionField: 'vacation2', mappingFunction: e => e.cust_vacationHazardous },
    { requisitionField: 'vacationnorth', mappingFunction: e => e.cust_vacationNothern },
    { requisitionField: 'vacationadd', mappingFunction: e => e.cust_vacationAdditional },
    { requisitionField: 'vacancyName', mappingFunction: e => e.externalName_defaultValue },
    { requisitionField: 'dep1', mappingFunction: e => e.cust_level1 },
    { requisitionField: 'dep2', mappingFunction: e => e.cust_level2 },
    { requisitionField: 'dep3', mappingFunction: e => e.cust_level3 },
    { requisitionField: 'dep4', mappingFunction: e => e.cust_level4 },
    { requisitionField: 'dep5', mappingFunction: e => e.cust_level5 },
    { requisitionField: 'dep6', mappingFunction: e => e.cust_level6 },
    { requisitionField: 'fte', mappingFunction: e => e.targetFTE },
    { requisitionField: 'cust_filter1', mappingFunction: e => e.cust_filter1 },
    { requisitionField: 'cove', mappingFunction: e => e.cust_cove },
    { requisitionField: 'monthlyPrem', mappingFunction: e => e.cust_monthlyPrem },
    { requisitionField: 'feedComp', mappingFunction: e => e.cust_feedComp },
    { requisitionField: 'numbotiz3', mappingFunction: e => e.cust_numbotiz3 },
    { requisitionField: 'numbotiz12', mappingFunction: e => e.cust_numbotiz12 },
    { requisitionField: 'numbotiz13', mappingFunction: e => e.cust_numbotiz13 },
    { requisitionField: 'projectBonus', mappingFunction: e => e.cust_projectBonus },
    { selectField: 'projectPosition/id', requisitionField: 'projectPosition', mappingFunction: e => ({id: e.cust_projectPositionNav? e.cust_projectPositionNav.label_en_DEBUG: null}), getter: e => e.projectPosition.results[0]?.id, newReqValueGetter: e => e ? e.id : null },
    { selectField: 'shiftWorker/id', requisitionField: 'shiftWorker', mappingFunction: e => ({id: e.cust_shiftWorkerNav ? e.cust_shiftWorkerNav.label_en_DEBUG : null}), getter: e => e.shiftWorker && e.shiftWorker.results[0] ? e.shiftWorker.results[0].id : null, newReqValueGetter: e => e ? e.id : null},
];

var additionalReqFieldsToSelect = [
    'jobReqId',
    'internalStatus',
    'templateId',
]

var requisitionExpands = [
    'shiftWorker',
    'projectPosition',
    'department_obj',
    'jobReqLocale',
    'legalEntity_obj',
];

/*
чего не мэппил:
harmFactors
harmFactors1
privatereq
hiringManager
coordinator
sourcer
vpOfStaffing
openDate
fulldep - в правиле че-то не указано откуда оно
*/
var positionFields = [
    'code',
    'cust_positionCode',
    //'cust_vacTypeStudentNav/externalCode',
    'externalName_defaultValue',
    'companyNav/externalCode',
    'companyNav/name_defaultValue',
    'jobCode',
    'cust_Grade',
    'cust_rank',
    'department',
    'cust_jobCategory',
    'departmentNav/cust_address',
    'cust_vacationHazardous',
    'cust_vacationNothern',
    'cust_vacationBase',
    'cust_vacationAdditional',
    'cust_level1',
    'cust_level2',
    'cust_level3',
    'cust_level4',
    'cust_level5',
    'cust_level6',
    'targetFTE',
    'cust_filter1',
    'cust_cove',
    'cust_monthlyPrem',
    'cust_feedComp',
    'cust_numbotiz3',
    'cust_numbotiz13',
    'cust_numbotiz12',
    'cust_projectBonus',
    'cust_projectPositionNav/label_en_DEBUG',
    'cust_shiftWorkerNav/label_en_DEBUG',
];

var positionExpands = [
    'companyNav',
    //'cust_vacTypeStudentNav',
    'departmentNav',
    'cust_projectPositionNav',
    'cust_shiftWorkerNav',
];

var changeList = [
    ['4533', 'e176fa25-ca42-11ea-912d-005056846fd2'],
];

function toEscapedString(arr) {
    return arr.map(e => `'${e}'`).join(',');
}

async function runProgram() {
    var reqToPos = {}
    changeList.forEach(e => reqToPos[e[0]] = e[1]);
    
    var requisitionIdList = changeList.map(e => e[0]);
    var newPositionIdList = changeList.map(e => e[1]);
    
    var positionSelectClause = positionFields.join(',');
    var positionExpandClause = positionExpands.join(',');
    var positionFilterClause = toEscapedString(newPositionIdList);

    var positionsQuery = `Position?$select=${positionSelectClause}&$expand=${positionExpandClause}&$filter=code in ${positionFilterClause}&paging=snapshot`;
    var positions = await getAll(positionsQuery);
    
    var requisitionSelectClause = additionalReqFieldsToSelect.concat(requisitionFields.map(f => f.selectField || f.requisitionField)).join(',');
    var requisitionExpandClause = requisitionExpands.join(',');
    var requisitionFilterClause = toEscapedString(requisitionIdList);
    var requisitionsQuery = `JobRequisition?$select=${requisitionSelectClause}&$expand=${requisitionExpandClause}&$filter=deleted ne 'Deleted' and jobReqId in ${requisitionFilterClause}&paging=snapshot`;
    var requisitions = await getAll(requisitionsQuery)
    
    positionsDict = {};
    positions.forEach(p => positionsDict[p.code] = p);
    
    var result = [];
    
    for (var r of requisitions) {
        var requisitionData = {
            jobReqId: r.jobReqId,
            internalStatus: r.internalStatus,
            templateId: r.templateId,
            fields: {}
        };
        var newPositionCode = reqToPos[r.jobReqId];
        var newPosition = positionsDict[newPositionCode];
        for (var field of requisitionFields) {
            requisitionData.fields[field.requisitionField] = {
                    current: typeof field.getter === 'function' ? field.getter(r) : r[field.requisitionField],
                    new: field.mappingFunction(newPosition)
                }
        }
        result.push(requisitionData);
    }
    
    function formatToCsv(res) {
        // header
        csvData = additionalReqFieldsToSelect.concat(requisitionFields.map(e => e.requisitionField)).join(',');
        csvData += "\n";
        // data
        res.forEach(req => {
            var line = additionalReqFieldsToSelect.map(f => req[f]).join(',') + ',';
            line += requisitionFields.map(e => {
                var fieldValues = req.fields[e.requisitionField];
                var oldValue = fieldValues.current ? fieldValues.current.toString().replace(/"/g,'""') : '';
                var newValue = typeof e.newReqValueGetter == 'function' ? e.newReqValueGetter(fieldValues.new) : fieldValues.new;
                newValue = newValue ? newValue.toString().replace(/"/g,'""') : '';
                return `"${oldValue} => ${newValue}"`;
            }).join(',');
            line += "\n";
            csvData += line;
        })
        return csvData;
    }
    
    fs.writeFileSync('report.csv', formatToCsv(result), 'utf8');
    alreadyMapped = result.filter(r => r.fields['positionNumber'].current === r.fields['positionNumber'].new);
    notMapped = result.filter(r => r.fields['positionNumber'].current !== r.fields['positionNumber'].new);
    fs.writeFileSync('alreadyMapped.csv', formatToCsv(alreadyMapped), 'utf8');

    var payload = [];
    for (var res of notMapped) {
        var record = {};
        record.__metadata = {
            uri:`JobRequisition(${res.jobReqId}L)`,
            type: 'SFOData.JobRequisition'};
        record.jobReqId = res.jobReqId;
        for (var field of requisitionFields) {
            record[field.requisitionField] = res.fields[field.requisitionField].new || null;
        }
        payload.push(record);
    }
    
    fs.writeFileSync('payload.json', JSON.stringify(payload, null, 4), 'utf8');
    if (!DRY_RUN) {
        await postAll(payload, 'upsert');
    }
}
runProgram();